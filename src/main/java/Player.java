public class Player {

    private int id;
    private String name;
    private int goals;

    public Player() { }

    public Player(int id, String name, int goals) {
        this.id = id;
        this.name = name;
        this.goals = goals;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", goals=" + goals +
                '}';
    }
}
