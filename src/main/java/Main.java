import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void sortTeams (List<Team> teams) {

        Comparator<Team> compareByPoints = Comparator.comparing(Team::getPoints);
        Comparator<Team> compareByGoals = Comparator.comparing(Team::getGoals);
        Comparator<Team> compareByPointsThenGoals = compareByPoints.thenComparing(compareByGoals);
        teams.sort(compareByPointsThenGoals);
    }

    public static void main(String[] args) {

        Player goalGetter = new Player();
        List<Team> leaderBoard = new ArrayList<Team>();

        Team steaua = new Team(1, "Steaua Bucuresti");
        Team dinamo = new Team(2, "Dinamo Bucuresti");
        Team barcelona = new Team(3, "FC Barcelona");
        Team realMadrid = new Team(4, "Real Madrid");

        steaua.addToPlayerList(new Player(1, "Nicolita", 0));
        steaua.addToPlayerList(new Player(2, "Banel", 0));
        steaua.addToPlayerList(new Player(3, "Jordi", 0));

        dinamo.addToPlayerList(new Player(4, "Danciulescu", 0));
        dinamo.addToPlayerList(new Player(5, "Ilie", 0));
        dinamo.addToPlayerList(new Player(6, "Dragomir", 0));

        barcelona.addToPlayerList(new Player(7, "Alba", 0));
        barcelona.addToPlayerList(new Player(8, "Neymar", 0));
        barcelona.addToPlayerList(new Player(9, "Messi", 0));

        realMadrid.addToPlayerList(new Player(10, "Ronaldo", 0));
        realMadrid.addToPlayerList(new Player(11, "Augustus", 0));
        realMadrid.addToPlayerList(new Player(12, "John", 0));

        Championship worldChampionship = new Championship(1);

        worldChampionship.addToMatchList(worldChampionship.generateMatch(steaua, dinamo));
        worldChampionship.addToMatchList(worldChampionship.generateMatch(steaua, realMadrid));
        worldChampionship.addToMatchList(worldChampionship.generateMatch(steaua, barcelona));

        worldChampionship.addToMatchList(worldChampionship.generateMatch(dinamo, barcelona));

        worldChampionship.addToMatchList(worldChampionship.generateMatch(barcelona, realMadrid));
        worldChampionship.addToMatchList(worldChampionship.generateMatch(barcelona, steaua));
        worldChampionship.addToMatchList(worldChampionship.generateMatch(barcelona, dinamo));

        goalGetter = worldChampionship.getChampionshipGoalGetter(worldChampionship.getMatchList());

        leaderBoard.add(steaua);
        leaderBoard.add(dinamo);
        leaderBoard.add(barcelona);
        leaderBoard.add(realMadrid);

        sortTeams(leaderBoard);

        System.out.println("Championship goal getter: " + goalGetter.toString());
        System.out.println("Leaderboard:");
        for (Team team : leaderBoard) {
            System.out.println(team.toString());
        }

        System.out.println("Matches:");
        for (Match match : worldChampionship.getMatchList()) {
            System.out.println(match.toString());
        }
    }
}
