import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Championship {

    private int id;
    private List<Match> matchList;

    public Championship() { }

    public Championship(int id) {
        this.id = id;
        matchList = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Match> getMatchList() {
        return matchList;
    }

    public void setMatchList(List<Match> matchList) {
        this.matchList = matchList;
    }

    public void addToMatchList(Match match) {
        this.matchList.add(match);
    }

    @Override
    public String toString() {
        return "Championship{" +
                "id=" + id +
                ", matchList=" + matchList +
                '}';
    }

    public Match generateMatch(Team firstTeam, Team secondTeam) {

        Random rand = new Random();
        int firstRandom = rand.nextInt(10);
        int secondRandom = rand.nextInt(10);

        if (firstRandom > secondRandom) {
            firstTeam.setPoints(firstTeam.getPoints() + 3);
        }
        else if (firstRandom < secondRandom) {
            secondTeam.setPoints(secondTeam.getPoints() + 3);
        }
        else {
            firstTeam.setPoints(firstTeam.getPoints() + 1);
            secondTeam.setPoints(secondTeam.getPoints() + 1);
        }

        return new Match(matchList.size(), firstTeam, secondTeam, firstRandom, secondRandom);
    }

    public Player getChampionshipGoalGetter(List<Match> matches) {

        Random rand = new Random();
        int maxGoals = 0;
        Player goalGetter = new Player();

        for (Match match : matches) {
            int playerIndex = rand.nextInt(match.getFirstTeam().getPlayerList().size());
            match.getFirstTeam().getPlayerList().get(playerIndex).setGoals(match.getFirstTeamScore());
            playerIndex = rand.nextInt(match.getSecondTeam().getPlayerList().size());
            match.getSecondTeam().getPlayerList().get(playerIndex).setGoals(match.getSecondTeamScore());

            if (match.getFirstTeam().getPlayerList().get(playerIndex).getGoals() > maxGoals) {
                maxGoals = match.getFirstTeam().getPlayerList().get(playerIndex).getGoals();
                goalGetter = match.getFirstTeam().getPlayerList().get(playerIndex);
            }
            else if (match.getSecondTeam().getPlayerList().get(playerIndex).getGoals() > maxGoals) {
                maxGoals = match.getSecondTeam().getPlayerList().get(playerIndex).getGoals();
                goalGetter = match.getSecondTeam().getPlayerList().get(playerIndex);
            }
            match.getFirstTeam().setGoals(match.getFirstTeam().getGoals() + match.getFirstTeamScore());
            match.getSecondTeam().setGoals(match.getSecondTeam().getGoals() + match.getSecondTeamScore());
        }

        return goalGetter;
    }
}
