import java.util.ArrayList;
import java.util.List;

public class Team {

    private int id;
    private String name;
    private List<Player> playerList;
    private int points;
    private int goals;

    public Team() { }

    public Team(int id, String name) {
        this.id = id;
        this.name = name;
        this.points = 0;
        this.goals = 0;
        playerList = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }

    public void addToPlayerList(Player player) {
        this.playerList.add(player);
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", playerList=" + playerList +
                ", points=" + points +
                ", goals=" + goals +
                '}';
    }
}
